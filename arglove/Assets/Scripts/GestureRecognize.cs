﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GestureRecognize : MonoBehaviour 
{
	public SensoHandsController SHController;
	private const string LittleFingerGesture = "gesture_0_start"; // мизинец вверх
	private const string PeaceGestureStart = "gesture_1_start"; // пис
	private const string PeaceGestureStop = "gesture_1_end";
	private const string FistGesture = "gesture_2_start"; // кулак
	// private const string RockGesture = "gesture_3_start"; // раскрытая ладонь
	// private const string RockGesture = "gesture_4_start";
	private const string RockGestureStart = "gesture_5_start"; // коза
	private const string RockGestureStop = "gesture_5_end";

	public UnityEvent PlayMusic, StopMusic, PlayStars, StopStars;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		switch(SHController.Gesture)
		{
			case PeaceGestureStart:
				PlayStars.Invoke();
				break;
			case PeaceGestureStop:
				StopStars.Invoke();
				break;
			case RockGestureStart:
				PlayMusic.Invoke();
				break;
			case RockGestureStop:
				StopMusic.Invoke();
				break;
		}
	}
}
