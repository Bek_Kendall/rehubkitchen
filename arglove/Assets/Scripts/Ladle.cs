﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ladle : MonoBehaviour {

	public Slider slider;
	public CheckFist Hand;
	public GameObject Particle;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider collision)
    {
		if (collision.gameObject.tag == "Finish" && Hand.Take && Hand.CurSphere.tag == "Ladle")
		{
			Debug.Log(222);
			StartCoroutine(Mixing());
		}
	}

	void OnTriggerExit(Collider collision)
	{
		if (collision.gameObject.tag == "Finish")
		{
			StopAllCoroutines();
			slider.value = 0;
		}
	}

	IEnumerator Mixing()
	{
		while (slider.value <= 99)
		{
			if (!Hand.Take) 
				yield break;
			slider.value += 0.5f;
			yield return new WaitForSeconds(0.05f);
		}
		slider.gameObject.SetActive(false);
		//ReadyText.text = "Готово";
		Particle.SetActive(true);
	}
}
