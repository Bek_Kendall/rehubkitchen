﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SphereController : MonoBehaviour
{
	public string Name;

	Vector3 StartPos;

	void Start()
	{
		StartPos = transform.position;
	}
	void OnTriggerEnter(Collider collision)
    {
		if (collision.gameObject.tag == "Finish")
		{
			if (tag != "Urn" && tag != "Ladle")
				gameObject.SetActive(false);
			// GetComponent<Renderer>().enabled = false;
			// yield return new WaitForSeconds(1.2f);
			// GetComponent<Renderer>().enabled = true;
			// transform.position = StartPos;
			// GetComponent<Renderer>().material.color = new Color(Random.value, Random.value, Random.value, 1);
		}
	}

	void OnCollisionEnter(Collision coll)
	{
		if (coll.gameObject.tag != "Urn" && coll.gameObject.tag != "Ladle")
		{
			transform.position = StartPos;
			GetComponent<Rigidbody>().velocity = Vector3.zero;
		}
	}
}
