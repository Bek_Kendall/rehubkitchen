﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Basket : MonoBehaviour 
{	
	public Slider slider;
	int Count = 1;
	public float Speed = 3.0f;
	public float Step = 0.02f;

	public List<string> Recipe;
	Vector3 newPos;
	public TextMesh RecipeText;

	//public Text ReadyText;
	public GameObject Particle;
	void Start()
	{
		newPos = transform.localPosition;
		Recipe = new List<string>{"Лук", "Грибы", "Морковь", "Чеснок", "Томат"};
		UpdateText();
	}
	void Update()
	{
		transform.localPosition = Vector3.MoveTowards(transform.localPosition, newPos, Speed * Time.deltaTime);
	}
	void OnTriggerEnter(Collider collision)
    {
		if (collision.gameObject.tag == "Sphere")
		{
			newPos.y += Step;
			Count++;
			string Name = collision.gameObject.GetComponent<SphereController>().Name;
			Recipe[Recipe.FindIndex(a => a == Name)] = "+ " + Name;
			UpdateText();
			if (Count >= 6f)
			{
				slider.gameObject.SetActive(true);
				//ReadyText.gameObject.SetActive(true);
				GetComponent<Collider>().isTrigger = false;
				RecipeText.text = "Овощной суп\n    Рецепт:\n";
				RecipeText.text += " Перемешать";
			}
			// StartCoroutine(Particle(collision.gameObject.GetComponent<Renderer>().material.color));
		}
	}

	// IEnumerator Particle(Color color)
	// {
	// 	var main = particle.main;
    //     main.startColor = color;
	// 	particle.gameObject.SetActive(true);
	// 	yield return new WaitForSeconds(1.0f);
	// 	particle.Stop();
	// }

	void UpdateText()
	{
		RecipeText.text = "Овощной суп\n    Рецепт:\n";
		foreach(string ingr in Recipe)
		{
			RecipeText.text += " " + ingr + "\n";
		}
	}
}
