﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Basket : MonoBehaviour 
{	
	int Count = 1;
	public CheckFist Hand;
	public float Speed = 3.0f;
	public float Step = 0.02f;
	public ParticleSystem particle;
	public List<string> Recipe;
	Vector3 newPos;
	public TextMesh RecipeText;
	public Slider slider;
	//public Text ReadyText;
	public GameObject Particle;
	void Start()
	{
		newPos = transform.localPosition;
		Recipe = new List<string>{"Лук", "Грибы", "Морковь", "Чеснок", "Томат"};
		UpdateText();
	}
	void Update()
	{
		transform.localPosition = Vector3.MoveTowards(transform.localPosition, newPos, Speed * Time.deltaTime);
	}
	void OnTriggerEnter(Collider collision)
    {
		if (collision.gameObject.tag == "Sphere")
		{
			newPos.y += Step;
			Count++;
			string Name = collision.gameObject.GetComponent<SphereController>().Name;
			Recipe[Recipe.FindIndex(a => a == Name)] = "+ " + Name;
			UpdateText();
			if (Count >= 6f)
			{
				slider.gameObject.SetActive(true);
				//ReadyText.gameObject.SetActive(true);
				GetComponent<Collider>().isTrigger = false;
			}
			// StartCoroutine(Particle(collision.gameObject.GetComponent<Renderer>().material.color));
		}

		else if (collision.gameObject.tag == "Ladle" && Hand.Take && Hand.CurSphere.tag == "Ladle")
		{
			Debug.Log(222);
			StartCoroutine(Mixing());
		}
	}

	void OnTriggerExit(Collider collision)
	{
		if (collision.gameObject.tag == "Ladle")
		{
			StopAllCoroutines();
			slider.value = 0;
		}
	}

	// IEnumerator Particle(Color color)
	// {
	// 	var main = particle.main;
    //     main.startColor = color;
	// 	particle.gameObject.SetActive(true);
	// 	yield return new WaitForSeconds(1.0f);
	// 	particle.Stop();
	// }

	void UpdateText()
	{
		RecipeText.text = "Овощной суп\n    Рецепт:\n";
		foreach(string ingr in Recipe)
		{
			RecipeText.text += " " + ingr + "\n";
		}
		RecipeText.text += " Перемешать";
	}

	IEnumerator Mixing()
	{
		while (slider.value <= 99)
		{
			if (!Hand.Take) 
				yield break;
			slider.value += 0.5f;
			yield return new WaitForSeconds(0.05f);
		}
		slider.gameObject.SetActive(false);
		//ReadyText.text = "Готово";
		Particle.SetActive(true);
	}
}
