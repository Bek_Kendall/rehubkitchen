﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conveyer : MonoBehaviour 
{
	public float WaitTime;
	public Vector3 startPos;
	public GameObject SpherePref;
	IEnumerator Start () 
	{
		while(true)
		{
			GameObject sphere = Instantiate(SpherePref, startPos, Quaternion.identity);
			switch(Random.Range(0, 4))
			{
				case 0: sphere.GetComponent<Renderer>().material.color = new Color(255, 0, 0, 1);
					break;
				case 1: sphere.GetComponent<Renderer>().material.color = new Color(0, 255, 0, 1);
					break;
				case 2: sphere.GetComponent<Renderer>().material.color = new Color(0, 0, 255, 1);
					break;
				case 3: sphere.GetComponent<Renderer>().material.color = new Color(255, 255, 0, 1);
					break;
			}

			yield return new WaitForSeconds(WaitTime);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
