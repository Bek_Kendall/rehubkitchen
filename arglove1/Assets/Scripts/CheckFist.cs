﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckFist : MonoBehaviour 
{
	public enum EnumHand { Right, Left };
	public EnumHand Hand;
	public SensoHandsController SHController;
	public bool Take = false;
	FixedJoint fixedJoint;

	private const string FistGesture = "gesture_2_start";
	private const string RockGestureStart = "gesture_5_start"; // мизинец вверх
	private const string PeaceGestureStart = "gesture_1_start"; // пис

	public GameObject CurSphere;

	Transform HandTransform;
	SphereCollider HandCollider;
	// Use this for initialization
	void Start () 
	{
		fixedJoint = transform.GetChild(0).GetComponent<FixedJoint>();
		HandTransform = transform.GetChild(0);
		HandCollider = GetComponent<SphereCollider>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!CheckGestTake())
		{				
			GetComponent<Collider>().enabled = true;
			if (Take)
			{		
				Take = false;
				fixedJoint.connectedBody = null;
				CurSphere.GetComponent<Collider>().enabled = true;
				if (CurSphere.tag == "Ladle")
					CurSphere.GetComponent<Collider>().isTrigger = false;
			}
		}
	}

	void OnTriggerStay(Collider coll)
	{
		if ((coll.tag == "Sphere" || coll.tag == "Ladle") && !Take)
		{
			CurSphere = coll.gameObject;
			Take = CheckGestTake();

			if (Take)
			{
				if (Hand == EnumHand.Right)
				{
					SHController.SendVibro(Senso.EPositionType.RightHand, Senso.EFingerType.Index, 100, 10);
					SHController.SendVibro(Senso.EPositionType.RightHand, Senso.EFingerType.Middle, 100, 10);
					SHController.SendVibro(Senso.EPositionType.RightHand, Senso.EFingerType.Third, 100, 10);
					SHController.SendVibro(Senso.EPositionType.RightHand, Senso.EFingerType.Little, 100, 10);
				}
				if (Hand == EnumHand.Left)
				{
					SHController.SendVibro(Senso.EPositionType.LeftHand, Senso.EFingerType.Index, 100, 10);
					SHController.SendVibro(Senso.EPositionType.LeftHand, Senso.EFingerType.Middle, 100, 10);
					SHController.SendVibro(Senso.EPositionType.LeftHand, Senso.EFingerType.Third, 100, 10);
					SHController.SendVibro(Senso.EPositionType.LeftHand, Senso.EFingerType.Little, 100, 10);
				}
				fixedJoint.connectedBody = CurSphere.GetComponent<Rigidbody>();
				CurSphere.GetComponent<Rigidbody>().useGravity = true;
				if (coll.tag != "Ladle")
					CurSphere.GetComponent<Collider>().enabled = false;
				else 
				{
					CurSphere.GetComponent<Rigidbody>().isKinematic = false;
					CurSphere.GetComponent<Collider>().isTrigger = true;
				}
			}
		}
	}

	void OnTriggerEnter(Collider coll)
	{
		if (CheckGestTake())
		{
			GetComponent<Collider>().enabled = false;
		}
	}

	public void HandGoToward()
	{
		HandTransform.Translate(0.2f, 0, 0);  
		Vector3 vect = HandCollider.center;
		vect.z = HandTransform.transform.localPosition.z - 0.02f;
		HandCollider.center = vect;
	}

	public void HandGoBack()
	{
		HandTransform.Translate(-0.2f, 0, 0);    
		Vector3 vect = HandCollider.center;
		vect.z = HandTransform.transform.localPosition.z - 0.02f;
		HandCollider.center = vect;
	}

	public bool CheckGestTake()
	{
		return SHController.Gesture == FistGesture || SHController.Gesture == RockGestureStart || SHController.Gesture == PeaceGestureStart;
	}
}
